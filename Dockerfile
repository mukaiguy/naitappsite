# pull base image
FROM python:3.7

# Enviroment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# WORKING DIRECTORY

WORKDIR /NaitD1


#Install Dependencies

COPY Pipfile Pipfile.lock /NaitD1/
RUN pip install pipenv && pipenv install --system

# Copy Project

COPY . /NaitD1/